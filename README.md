# Natural Language Processing (NLP)

This repository contains three projects within the field of natural language processing using Python:
* Exploratory text analysis
* Word embeddings
* Named entity recognition (NER) with deep and transfer learning

Used libraries are among others:
* Wikipediaapi (data collection)
* Pandas (data analysis)
* Matplotlib, Plotly & Seaborn (data visualization)
* NLTK & SpaCy (natural language processing)
* Scikit-learn (machine learning)
* Gensim (pre-trained word embeddings)
* PyTorch (deep learning/neural networks)
* Transformers (transfer learning)

